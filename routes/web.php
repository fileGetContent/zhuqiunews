<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::any('test', 'TestController@index');
Route::any('queue', 'TestController@queue');
Route::any('git', 'TestController@git');


Route::group(['namespace' => 'Admin', 'prefix' => 'admin'], function () {

    Route::match(['get', 'post'], 'admin/login', 'AdminController@login')->name('admin.AdminController.login');

    Route::match(['get', 'post'], 'public/captcha', 'PublicController@captcha')->name('admin.PublicController.captcha');


    Route::group(['middleware' => 'admin.login'], function () {
        Route::match(['get', 'post'], 'admin/index', 'AdminController@index')->name('admin.AdminController.index');
        Route::match(['get', 'post'], 'admin/out', 'AdminController@out')->name('admin.AdminController.out');
        Route::match(['get', 'post'], 'admin/modify', 'AdminController@modify')->name('admin.AdminController.modify');

        Route::match(['get', 'post'], 'public/menu', 'PublicController@menu')->name('admin.PublicController.menu');

        Route::match(['get', 'post'], 'news/lists', 'NewsController@lists')->name('admin.NewsController.lists');
        Route::match(['get', 'post'], 'news/del', 'NewsController@del')->name('admin.NewsController.del');
        Route::match(['get', 'post'], 'news/grab', 'NewsController@grab')->name('admin.NewsController.grab');
        Route::match(['get', 'post'], 'news/update', 'NewsController@update')->name('admin.NewsController.update');
    });


});
