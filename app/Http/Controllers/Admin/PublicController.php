<?php

namespace App\Http\Controllers\Admin;

use App\Methods\Captcha;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class PublicController extends Controller
{
    /**
     * 图像二维码
     */
    public function captcha()
    {
        Captcha::Captcha();
    }


    /**
     * 网站栏目
     */
    public function menu()
    {
        $spread   = false;
        $menudata = [
//            [
//                'title' => '统计数据', 'icon' => 'icon-huiyuanguanli1', 'spread' => $spread, 'list' => [
//                ['title' => '会员数据', 'icon' => 'icon-huiyuanguanli2', 'url' => url('/admin/Statistics/user'), 'spread' => $spread],
//                ['title' => '种植地', 'icon' => 'icon-huiyuanguanli2', 'url' => url('/admin/Statistics/lands'), 'spread' => $spread],
//                ['title' => '资产数据', 'icon' => 'icon-huiyuanguanli2', 'url' => url('/admin/Statistics/asset'), 'spread' => $spread],
//            ],
//            ],
            [
                'title' => '足球新闻', 'icon' => 'icon-huiyuanguanli1', 'spread' => $spread, 'list' => [
                ['title' => '足球列表', 'icon' => 'icon-huiyuanguanli2', 'url' => route('admin.NewsController.lists'), 'spread' => $spread],
            ],
            ],
            [
                'title' => '系统设置', 'icon' => 'icon-huiyuanguanli1', 'spread' => $spread, 'list' => [
                ['title' => '修改密码', 'icon' => 'icon-huiyuanguanli2', 'url' => route('admin.AdminController.modify'), 'spread' => $spread],
            ],
            ],
        ];
        return json_encode([
            'code' => 0,
            'msg'  => '',
            'data' => $menudata,
        ]);
    }
}
