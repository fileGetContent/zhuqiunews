<?php

namespace App\Http\Controllers\Admin;

use App\Jobs\NewsInsert;
use App\News;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class NewsController extends Controller
{
    /**
     * 足球新闻
     * @param Request $request
     * @return \Illuminate\Contracts\View\Factory
     */
    public function lists(Request $request)
    {
        if ($request->ajax()) {
            $all   = $request->all();
            $where = [];

            if ($request->has('title') && $request->input('title', '') != '') {
                $where [] = ['title', 'like', $all['title'] . '%'];
            }

            if ($request->has('time') && $request->input('time', '') != '') {
                $where [] = ['update_time', '>=', strtotime($all['time'])];
            }
            $data   = (new  News)->setTable('news2019_07')->where($where)->orderBy('update_time', 'desc')
                ->forPage($all['page'], $all['limit'])
                ->get()->toArray();
            $count = (new  News)->setTable('news2019_07')->where($where)->count();
            return self::layuiPageOK($data, $count);
        }
        return view('Admin.NewsLists');
    }


    /**
     * 删除
     * @param Request $request
     * @return false|string
     */
    public function del(Request $request)
    {
        $bool = News::where('id', '=', $request->input('id'))->delete();
        if ($bool) {
            return self::ok();
        }
        return self::failed();
    }

    /**
     * 采集任务
     * @param Request $request
     * @return false|\Illuminate\Contracts\View\Factory|\Illuminate\View\View|string
     */
    public function grab(Request $request)
    {
        if ($request->ajax()) {
            if ($request->has('h')) {
                $res  = file_get_contents('https://m.zhibo8.cc/json/hot/24hours.htm'); // 采集基本信息列表 $data['news']
                $data = json_decode($res, true);
                foreach ($data['news'] as $k => $value) {
                    NewsInsert::dispatch($value);
                }
            } else {
                $key  = rand(99999999, 10000000) . rand(99999999, 10000000);
                $url  = 'https://news.zhibo8.cc/zuqiu/json/' . $request->input('date') . '.htm?key=0.' . $key;
                $res  = file_get_contents($url); // 采集基本信息列表 $data['video_arr']
                $data = json_decode($res, true);
                foreach ($data['video_arr'] as $k => $value) {
                    NewsInsert::dispatch($value);
                }
            }

            return self::ok();
        }
        return view('Admin.NewsGrad');
    }


    /**
     * 新闻更新
     * @param Request $request
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View|string
     */
    public function update(Request $request)
    {
        $data =(new  News)->setTable('news2019_07')->where('id', '=', $request->input('id'))->first();
        if (is_null($data)) {
            return '未找到';
        }

        return view('Admin.NewsUpdate', [
            'data' => $data
        ]);
    }


}
