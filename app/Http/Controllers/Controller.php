<?php

namespace App\Http\Controllers;

use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;

class Controller extends BaseController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;


    /**
     * 成功
     * @param $message
     * @param $data
     */
    public static function ok($message = '操作成功', $data = '')
    {
        return json_encode([
            'status'  => 'Success',
            'message' => $message,
            'result'  => $data,
        ]);
    }

    /**
     * 请求失败
     * @param $message
     */
    public static function failed($message = '操作失败')
    {
        return json_encode([
            'status'  => 'Failed',
            'message' => $message,
        ], true);
    }

    /**
     * layui分页
     * @param $list
     * @param $count
     * @param $message
     * @return \think\response\Json
     */
    public static function layuiPageOK($list, $count, $message = '')
    {
        return json_encode([
            'status'    => 'Success',
            'message'   => $message,
            'datalist'  => $list,
            'datacount' => $count,
        ]);
    }
}
