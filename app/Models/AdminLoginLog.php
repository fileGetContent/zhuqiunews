<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * @property-read \Illuminate\Notifications\DatabaseNotificationCollection|\Illuminate\Notifications\DatabaseNotification[] $notifications
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\AdminMenus newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\AdminMenus newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\AdminMenus query()
 * @mixin \Eloquent
 */
class AdminLoginLog extends Model
{

    public $timestamps = false;
}
