<!DOCTYPE html>
<html>
<head>
    @include('Admin.Heard')
</head>
<body class="childrenBody" style="font-size: 12px;margin: 10px;">
<form class="layui-form" id="showPic">
    <input type="text" name="id" hidden value="{$id}"/>
    <input type="text" name="type" hidden value="{$type}"/>
    <div class="layui-row">
        <div class="layui-col-xs12 layui-col-md12">

            <div class="layui-form-item">
                <label class="layui-form-label">标题</label>
                <div class="layui-input-block">
                    <input type="text" name="title" value="{{$data->title}}" lay-verify="title" autocomplete="off"
                           placeholder="请输入标题" class="layui-input">
                </div>
            </div>

            <div class="layui-form-item">
                <label class="layui-form-label">来源</label>
                <div class="layui-input-block">
                    <input type="text" name="title" value="{{$data->from}}" lay-verify="title" autocomplete="off"
                           placeholder="请输入标题" class="layui-input">
                </div>
            </div>

            <div class="layui-inline">
                <label class="layui-form-label">创建日期</label>
                <div class="layui-input-inline">
                    <input type="text" name="date" value="{{$data->update_time}}" id="date" lay-verify="date"
                           placeholder="yyyy-MM-dd" autocomplete="off" class="layui-input">
                </div>
            </div>


            <div class="layui-form-item layui-form-text">
                <label class="layui-form-label">新闻内容</label>
                <div class="layui-input-block">
                    <textarea id="container" name="content" lay-verify="content"
                              class="layui-textarea">{!! $data->content !!}</textarea>
                </div>
            </div>


{{--            <div class="layui-form-item" style="text-align: center;">--}}
{{--                <button class="layui-btn" lay-submit="" lay-filter="save">保存</button>--}}
{{--            </div>--}}
        </div>
    </div>
</form>
<script type="text/javascript">
    layui.use(['form', 'layer', 'jquery', 'common', 'element', 'layedit'], function () {
        var $ = layui.jquery,
            form = layui.form,
            element = layui.element,
            layedit = layui.layedit,
            common = layui.common;

        layedit.set({
            uploadImage: {
                url: "{php} echo $url {/php}",//上传接口url
                type: 'post' //默认post
            }
        });
        var layeditIndex = layedit.build('container'); //建立编辑器
        /**保存*/
        form.verify({
            content: function (value) {
                layedit.sync(layeditIndex);
            },

        });
        /**保存*/
        form.on("submit(save)", function (data) {
            common.formSave("{:url('admin/Articles/activityUpdate')}", data.field);
            return false;
        });
    });
</script>
</body>
</html>
