<!DOCTYPE html>
<html>
<head>
    @include('Admin.Heard')
    <link rel="stylesheet" href="{{asset('static/admin/css/login.css')}}" media="all"/>
</head>
<body>
<div class="bgpic" style="">
    <div class="login">
        <h1>登录后台</h1>
        <form class="layui-form" action="" method="post">
            <div class="layui-form-item">
                <input class="layui-input" id="username" autocomplete="off" placeholder="登录账号" lay-verify="required"
                       type="text">
            </div>
            <div class="layui-form-item">
                <input class="layui-input" id="password" placeholder="登录密码" lay-verify="required" type="text"
                       onfocus="this.type='password'" autocomplete="off">
            </div>
            <div class="layui-form-item">
                <div class="layui-row">
                    <div class="layui-col-xs7">
                        <input type="text" id="vcode" lay-verify="required" placeholder="图形验证码" class="layui-input">
                    </div>
                    <div class="layui-col-xs5">
                        <div style="margin-left: 10px;">
                            <img id="captcha" src="{{route('admin.PublicController.captcha')}}" width="100%"
                                 height="38px"
                                 class="login_codeimg">
                        </div>
                    </div>
                </div>
            </div>
            <button class="layui-btn login_btn" lay-submit="" lay-filter="login">登录</button>
        </form>
        <p class="medbk">2013-{{date('Y')}} © 版权所有</p>
    </div>
</div>
</body>
<script type="text/javascript">

    layui.use(['form', 'layer', 'jquery'], function () {
        var $ = layui.jquery,
            form = layui.form;
        /**监听登陆提交*/
        form.on("submit(login)", function (data) {
            var username = $('#username').val();
            var password = $('#password').val();
            var vcode = $('#vcode').val();
            if (username.length < 1 || username.length > 30) {
                layer.msg('请输入正确的登录账号');
                return false;
            }
            if (password.length < 6 || password.length > 30) {
                layer.msg('请输入正确的登录密码');
                return false;
            }
            //弹出loading
            var loginLoading = top.layer.msg('登陆中，请稍候', {icon: 16, time: false, shade: 0.8});
            var loginurl = '{{route('admin.AdminController.login')}}?st=' + (new Date().getTime());
            var postdata = {'username': username, 'password': password, 'vcode': vcode};
            $.ajax({
                url: loginurl,
                dataType: 'json',
                type: 'post',
                data: postdata,
                error: function () {
                    layer.msg('网络错误,请检查网络是否正常');
                },
                success: function (data) {
                    //登陆成功
                    if (data.status == 'Success') {
                        window.location.href = data.result;
                        top.layer.close(loginLoading);
                    } else {
                        top.layer.close(loginLoading);
                        layer.msg(data.message);
                    }
                }
            });
            return false;
        })
    });


    $('#captcha').click(function () {
        $(this).attr('src', '{{route('admin.PublicController.captcha')}}?time=' + Math.random())
    })
</script>

</html>
