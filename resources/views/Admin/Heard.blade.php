<meta charset="utf-8">
<title>后台管理</title>
<meta name="renderer" content="webkit">
<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
<link rel="shortcut icon" href="{{asset('/favicon.ico')}}">
<meta name="viewport"
      content="width=device-width, initial-scale=1.0, minimum-scale=1.0, maximum-scale=1.0, user-scalable=0">
<link rel="stylesheet" href="{{asset('static/admin/layuiadmin/layui/css/layui.css')}}" media="all">
<link rel="stylesheet" href="{{asset('static/admin/layuiadmin/style/admin.css')}}" media="all">
<script type="text/javascript" charset="utf-8" src="{{asset('static/admin/js/jquery.min.js')}}"></script>
<script src="{{asset('static/admin/layuiadmin/layui/layui.js')}}"></script>
<script>
    layui.config({
        base: "{{asset('static/admin/layuiadmin/')}}/",
    }).extend({
        index: 'lib/index' //主入口模块
    }).use('index');
</script>
