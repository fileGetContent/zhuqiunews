<!DOCTYPE html>
<html>
<head>
    @include('Admin.Heard')
</head>
<body class="childrenBody" style="font-size: 12px;margin: 10px;">
<form class="layui-form" id="showPic">
    <div class="layui-row">
        <div class="layui-col-xs12 layui-col-md12">

            <div class="layui-inline">
                <label class="layui-form-label">按照日期选择抓取</label>
                <div class="layui-input-inline">
                    <input type="text" name="date" class="layui-input" id="test1" placeholder="yyyy-MM-dd">
                </div>
            </div>
            <div class="layui-form-item" pane="">
                <label class="layui-form-label">24小时新闻</label>
                <div class="layui-input-block">
                    <input type="checkbox" name="h[]" value="24" title="24小时新闻" checked="">
                </div>
                <div>勾选 24小时新闻 则不执行 按照日期选择抓取</div>
            </div>

            <div class="layui-form-item" style="text-align: center;">
                <button class="layui-btn" lay-submit="" id="save" lay-filter="save">抓取</button>
            </div>
        </div>
    </div>
</form>
<script type="text/javascript">
    layui.use(['form', 'layer', 'jquery', 'common', 'element', 'layedit', 'laydate'], function () {
        var $ = layui.jquery,
            form = layui.form,
            element = layui.element,
            layedit = layui.layedit,
            laydate = layui.laydate,
            common = layui.common;

        //常规用法
        laydate.render({
            elem: '#test1'
        });

        layedit.set({
            uploadImage: {
                url: "{php} echo $url {/php}",//上传接口url
                type: 'post' //默认post
            }
        });


        /**保存*/
        form.on("submit(save)", function (data) {
            $('#save').attr('disabled', true);
            $.ajax({
                type: 'post',
                data: data.field,
                dataType: 'json',
                url: "{{route('admin.NewsController.grab')}}",
                success: function (data) {
                    layer.msg('后台已添加采集任务');
                    $('#save').attr('disabled', false);
                }, error: function () {

                }
            });



            return false;
        });
    });
</script>
</body>
</html>
