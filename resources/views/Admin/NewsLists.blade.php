<!DOCTYPE html>
<html>
<head>
    @include('Admin.Heard')
</head>
<body>
<div class="layui-fluid">
    <div class="layui-card">
        <div class="layui-form layui-card-header layuiadmin-card-header-auto">
        </div>
        <div style="padding-bottom: 10px;">
            <a onclick="layui.common.layOpen('抓取', '{{route('admin.NewsController.grab')}}','100%','100%')"
               href="javascript:;" class="layui-btn"><i class="layui-icon">&#xe608;</i>抓取</a>
            {{--            <a onclick="layui.common.layOpen('本地添加', '{:url('admin/Articles/activityUpdate')}?type=1','100%','100%')"--}}
            {{--               href="javascript:;" class="layui-btn"><i class="layui-icon">&#xe608;</i>本地添加</a>--}}
        </div>


        <form class="layui-form" action="" onsubmit="return false">
            <blockquote class="layui-elem-quote activeTable  layui-text">
                <div class="my-btn-box">
                    <div style="color: red" class="layui-input-inline">
                        <input type="text" class="layui-input" id="test1" placeholder="按更新日期">
                    </div>

                    <div class="layui-input-inline" style="width: 240px;">
                        <input type="text" class="layui-input" id="title" placeholder="标题">
                    </div>

                    <button class="layui-btn mgl-20" data-type="search"><i class="layui-icon larry-icon"></i>搜索
                    </button>

                </div>
            </blockquote>
        </form>


        <table id="LAY-user-manage" lay-filter="LAY-user-manage"></table>
    </div>
</div>
</div>

<!-- 自定义模板 -->
@verbatim
    <script type="text/html" id="tp_avatar">
        {{# if(d.avatar){ }}
        <a href="javascript:;" onclick="layui.common.showPic()">
            <img layer-src="{{d.avatar}}" src="{{d.avatar}}" width="100%" height="100%">
        </a>
        {{# }  }}
    </script>
@endverbatim
<!--工具条 -->
<script type="text/html" id="tb_action">
    <a href="javascript:;" class="layui-btn layui-btn-xs" title="更新" lay-event="update">查看</a>
    <a href="javascript:;" class="layui-btn layui-btn-xs" title="删除" lay-event="del">删除</a>
</script>
<script>

    layui.use(['table', 'form', 'layer', 'jquery', 'common', 'laydate'], function () {
        var $ = layui.jquery,
            form = layui.form,
            table = layui.table,
            layer = layui.layer,
            laydate = layui.laydate,
            common = layui.common;

        /**数据表加载*/
        table.render({
            elem: '#LAY-user-manage',
            id: 'list',
            url: "{{route('admin.NewsController.lists')}}",
            response: {
                statusName: 'status', //数据状态的字段名称，默认：code
                statusCode: 'Success', //成功的状态码，默认：0
                msgName: 'message', //状态信息的字段名称，默认：msg
                countName: 'datacount', //数据总数的字段名称，默认：count
                dataName: 'datalist' //数据列表的字段名称，默认：data
            },
            page: true,
            limit: 10, // 初始每页显示条数
            cols: [[
                {field: 'id', title: 'ID', width: 80, sort: true, fixed: true},
                {field: 'title', title: '标题', width: 500},
                {field: 'from', title: '来源', width: 170},
                {field: 'label', title: '标签', width: 600},
                {field: 'update_time', title: '更新时间', width: 170},
                {fixed: 'right', title: '操作', align: 'center', width: 130, toolbar: '#tb_action'},
            ]]
        });
        /**监听工具条*/
        table.on('tool(LAY-user-manage)', function (obj) {
            switch (obj.event) {
                case 'update': // 编辑
                    var url = '{{route("admin.NewsController.update")}}?id=' + obj.data.id;
                    common.layOpen('查看', url, '100%', '100%');
                    break;
                case 'del':
                    var url = "{{route('admin.NewsController.del')}}";
                    // common.ajaxConfirm2('系统提示', '确定删除该数据吗？', url, {id: obj.data.id});
                    layer.confirm('系统提示', {
                        title: '确定删除该数据吗',
                        resize: false,
                        btn: ['确定', '取消'],
                        btnAlign: 'c',
                        icon: 3
                    }, function (index) {
                        $.ajax({
                            url: url,
                            type: 'post',
                            data: {id: obj.data.id},
                            async: false,
                            //  dataType: 'json',
                            success: function (data) {
                                layer.msg('删除成功');
                                location.reload()
                            }, error: function (data) {

                            }
                        });

                    }, function () {

                    });

                    break;
            }
        });

        //常规用法
        laydate.render({
            elem: '#test1'
        });

        // 数据操作
        var active = {
            search: function () {
                table.reload('list', {
                    where: {
                        title: $('#title').val(),
                        time: $('#test1').val()
                    }, page: {
                        curr: 1
                    }
                });
            }
        };

        $('.activeTable .layui-btn').on('click', function () {
            var type = $(this).data('type');
            active[type] ? active[type].call(this) : '';
        });

    });
</script>
</body>
</html>
