<!DOCTYPE html>
<html>
<head>
    @include('Admin.Heard')
</head>
<body class="childrenBody" style="font-size: 12px;margin: 10px;">
<form class="layui-form" id="showPic">
    <div class="layui-row">
        <div class="layui-col-xs12 layui-col-md12">
            <div class="layui-form-item">
                <label class="layui-form-label">用户名</label>
                <div class="layui-input-inline">
                    <input value="{{$admin->username}}" name="username" type="text" class="layui-input">
                </div>
            </div>
            <div class="layui-form-item">
                <label class="layui-form-label">密码</label>
                <div class="layui-input-inline">
                    <input type="password" class="layui-input" id="password" name="password" placeholder="登陆密码">
                </div>
                <div class="layui-form-mid layui-word-aux">不修改请留空</div>
            </div>
            <div class="layui-form-item" style="text-align: center;">
                <button class="layui-btn" id="save" lay-submit="" lay-filter="save">保存</button>
            </div>
        </div>
    </div>
    <input type="hidden" name="id" value="{{$admin->id}}">
</form>
<script src="{{asset('static/admin/layuiadmin/modules/common.js')}}"></script>
<script type="text/javascript">
    layui.use(['form', 'jquery'], function () {
        var $ = layui.jquery,
            form = layui.form,
            common = layui.common;

        $('#password').bind('blur', function () {
            if ($(this).val()) {
                $(this).attr('lay-verify', 'require|pass');
            } else {
                $(this).attr('lay-verify', '');
            }
        });

        form.verify({
            pass: [
                /^[\S]{6,12}$/,
                '密码必须6到12位，且不能出现空格'
            ],
            repass: function (value, item) {
                var password = $("#password").val();
                if (password != value) {
                    return '两次密码不一致';
                }
            }
        });

        /**保存*/
        form.on("submit(save)", function (data) {
            //  common.formSave("{{route('admin.AdminController.modify')}}", data.field);
            var loading = layer.msg('数据提交中，请稍候', {icon: 16, time: false, shade: 0.8});
            $('#save').attr('disabled', true);
            $.ajax({
                type: 'post',
                data: data.field,
                dataType: 'json',
                url: "{{route('admin.AdminController.modify')}}",
                success: function (data) {
                    layer.close(loading);
                    if (data.status == 'Success') {
                        layer.msg('操作成功');
                        location.reload()
                    } else {
                        layer.msg('操作失败');
                    }
                    $('#save').attr('disabled', false);
                }, error: function () {
                    layer.close(loading);
                }
            });

            return false;
        });
    });
</script>
</body>
</html>
