<!DOCTYPE html>
<html>
<head>
    @include('Admin.Heard')
</head>
<body class="layui-layout-body">

<div id="LAY_app">
    <div class="layui-layout layui-layout-admin">
        <div class="layui-header">
            <!-- 头部区域 -->
            <ul class="layui-nav layui-layout-left">
                <li class="layui-nav-item layadmin-flexible" lay-unselect>
                    <a href="javascript:;" layadmin-event="flexible" title="侧边伸缩">
                        <i class="layui-icon layui-icon-shrink-right" id="LAY_app_flexible"></i>
                    </a>
                </li>
                <!--  <li class="layui-nav-item layui-hide-xs" lay-unselect>
                   <a href="http://www.layui.com/admin/" target="_blank" title="前台">
                     <i class="layui-icon layui-icon-website"></i>
                   </a>
                 </li> -->
                <li class="layui-nav-item" lay-unselect>
                    <a href="javascript:;" layadmin-event="refresh" title="刷新">
                        <i class="layui-icon layui-icon-refresh-3"></i>
                    </a>
                </li>
                <!-- <li class="layui-nav-item layui-hide-xs" lay-unselect>
                  <input type="text" placeholder="搜索..." autocomplete="off" class="layui-input layui-input-search" layadmin-event="serach" lay-action="template/search.html?keywords=">
                </li> -->
            </ul>
            <ul class="layui-nav layui-layout-right" lay-filter="layadmin-layout-right">

                <!--   <li class="layui-nav-item" lay-unselect>
                    <a lay-href="app/message/index.html" layadmin-event="message" lay-text="消息中心">
                      <i class="layui-icon layui-icon-notice"></i>

                      <span class="layui-badge-dot"></span>
                    </a>
                  </li> -->
                <li class="layui-nav-item layui-hide-xs" lay-unselect>
                    <a href="javascript:;" layadmin-event="theme">
                        <i class="layui-icon layui-icon-theme"></i>
                    </a>
                </li>
                <!--   <li class="layui-nav-item layui-hide-xs" lay-unselect>
                    <a href="javascript:;" layadmin-event="note">
                      <i class="layui-icon layui-icon-note"></i>
                    </a>
                  </li> -->
                <li class="layui-nav-item layui-hide-xs" lay-unselect>
                    <a href="javascript:;" layadmin-event="fullscreen">
                        <i class="layui-icon layui-icon-screen-full"></i>
                    </a>
                </li>
                <li class="layui-nav-item" lay-unselect>
                    <a href="javascript:;">
                        <cite>{{session('admin_name')}}</cite>
                    </a>
                    <dl class="layui-nav-child">
                        <!-- <dd><a lay-href="set/user/info.html">基本资料</a></dd> -->
{{--                        <dd style="text-align: center;"><a href="javascript:;"--}}
{{--                                                           onclick="layui.common.layOpen('编辑管理员', '{:url('admin/admin.Admin/edit')}?id={$admin_id}','500px','500px')"><i--}}
{{--                                    class="iconfont icon-zhanghu" data-icon="icon-zhanghu"></i><cite>修改</cite></a></dd>--}}
{{--                        <hr>--}}
                        <dd id="logout" style="text-align: center;"><a>退出</a></dd>
                    </dl>
                </li>

                <!--        <li class="layui-nav-item layui-hide-xs" lay-unselect>
                         <a href="javascript:;" layadmin-event="about"><i class="layui-icon layui-icon-more-vertical"></i></a>
                       </li>
                       <li class="layui-nav-item layui-show-xs-inline-block layui-hide-sm" lay-unselect>
                         <a href="javascript:;" layadmin-event="more"><i class="layui-icon layui-icon-more-vertical"></i></a>
                       </li> -->
            </ul>
        </div>

        <!-- 侧边菜单 -->

        <div class="layui-side layui-side-menu">
            <div class="layui-logo" lay-href="{:url('admin/main.Main/index')}">
                <span>后台管理</span>
            </div>
            <!-- <div class="navBar layui-side-scroll"></div> -->

{{--        <!-- 建议改变真实接口地址时保留lay-url中的?v=@{{ layui.admin.v }} -->--}}

            <script type="text/html" template lay-url="{{route('admin.PublicController.menu')}}?v=@{{ layui.admin.v }}"
                    lay-done="layui.element.render('nav', 'layadmin-system-side-menu');" id="TPL_layout">
                <div lay-templateid="TPL_layout">
                    @verbatim
                    <ul class="layui-nav layui-nav-tree" lay-shrink="all" id="LAY-system-side-menu" lay-filter="layadmin-system-side-menu">
                        {{#
                        var dataName = layui.setter.response.dataName ? layui.setter.response.dataName : 'data';
                        layui.each(d[dataName], function(index,item){
                        }}
                        <li class="layui-nav-item {{ item.spread ? typeof item.list === 'object' && item.list.length > 0 ? 'layui-nav-itemed' : 'layui-this': '' }}"
                            myDir="1">
                            {{# if(item.url){ }}
                            <a lay-href="{{ item.url }}" lay-tips="{{ item.title }}" lay-direction="2">
                                {{# }else{ }}
                                <a href="javascript:;" lay-tips="{{ item.title }}" lay-direction="2">
                                    {{# } }}
                                    {{# if(item.icon){ }}
                                    <i class="layui-icon {{ item.icon }}"></i>
                                    {{# } }}
                                    <cite>{{ item.title }}</cite>
                                </a>
                                {{#
                                var itemListFun = function(itemList, myDir){
                                myDir = myDir || 2;
                                if(typeof itemList === 'object' && itemList.length > 0){ }}
                                <dl class="layui-nav-child">
                                    {{# layui.each(itemList, function(index2, item2){ }}
                                    <dd data-name="{{ item2.name || '' }}"
                                        class="{{ item2.spread ? (typeof item2.list === 'object' && item2.list.length > 0 ? 'layui-nav-itemed' : 'layui-this' ) : ''}}"
                                        myDir="{{ myDir }}">
                                        {{# if(item2.icon){ }}
                                        <i class="layui-icon {{ item.icon }}"></i>
                                        {{# } }}
                                        {{# if(item2.url){ }}
                                        <a lay-href="{{ item2.url }}">{{ item2.title }}</a>
                                        {{# }else{ }}
                                        <a href="javascript:;">{{ item2.title }}</a>
                                        {{# } }}
                                        {{# itemListFun(item2.list,myDir+1);}}
                                    </dd>
                                    {{# }) }}
                                </dl>
                            {{# } }}
                            {{# };
                            itemListFun(item.list); }}
                        </li>
                        {{# }) }}
                    </ul>
                        @endverbatim
                </div>
            </script>

        </div>


        <!-- 页面标签 -->
        <div class="layadmin-pagetabs" id="LAY_app_tabs">
            <div class="layui-icon layadmin-tabs-control layui-icon-prev" layadmin-event="leftPage"></div>
            <div class="layui-icon layadmin-tabs-control layui-icon-next" layadmin-event="rightPage"></div>
            <div class="layui-icon layadmin-tabs-control layui-icon-down">
                <ul class="layui-nav layadmin-tabs-select" lay-filter="layadmin-pagetabs-nav">
                    <li class="layui-nav-item" lay-unselect>
                        <a href="javascript:;"></a>
                        <dl class="layui-nav-child layui-anim-fadein">
                            <dd layadmin-event="closeThisTabs"><a href="javascript:;">关闭当前标签页</a></dd>
                            <dd layadmin-event="closeOtherTabs"><a href="javascript:;">关闭其它标签页</a></dd>
                            <dd layadmin-event="closeAllTabs"><a href="javascript:;">关闭全部标签页</a></dd>
                        </dl>
                    </li>
                </ul>
            </div>
            <div class="layui-tab" lay-unauto lay-allowClose="true" lay-filter="layadmin-layout-tabs">
                <ul class="layui-tab-title" id="LAY_app_tabsheader">
                    <li lay-id="{:url('admin/main.Main/index')}" lay-attr="{:url('admin/main.Main/index')}"
                        class="layui-this"><i class="layui-icon layui-icon-home"></i></li>
                </ul>
            </div>
        </div>


        <!-- 主体内容 -->
        <div class="layui-body" id="LAY_app_body">
            <div class="layadmin-tabsbody-item layui-show">
                <iframe src="{:url('admin/Index/home')}" frameborder="0" class="layadmin-iframe"></iframe>
            </div>
        </div>


        <!-- 辅助元素，一般用于移动设备下遮罩 -->
        <div class="layadmin-body-shade" layadmin-event="shade"></div>
    </div>
</div>


<script>
    var $, tab, skyconsWeather;
    layui.use(['form', 'element', 'layer', 'jquery'], function () {
        var form = layui.form,
            layer = layui.layer,
            element = layui.element;
        $ = layui.jquery;


        //退出
        $('#logout').on('click', function () {
            parent.layer.confirm('你真的确定要退出系统吗？', {
                title: '退出登陆提示！',
                resize: false,
                btn: ['确定退出系统', '不，我点错了！'],
                btnAlign: 'c',
                icon: 3
            }, function () {
                location.href = "{{route('admin.AdminController.out')}}";
            }, function () {

            })
        })
    })

</script>


</body>
</html>


