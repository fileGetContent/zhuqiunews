/**自定义模块*/
layui.define(['layer','jquery','form'], function (exports) {
	var $ = layui.jquery,
		form = layui.form;
	var $ = layui.jquery,
		Area = function(){
			this.areaConfig = {
				provId : 0,  //编辑需要的省
				cityId : 0,  //编辑需要的市
				distId : 0,  //编辑需要的县/区
				url : '/api/get_area'  //数据请求地址
			}
		};
	
	form.on('select(area)', function(data){
		$that = $(data.elem);
		urls = AreaO.areaConfig.url+"?area_id="+data.value;
		AreaO.getJSON(urls,$that.closest("div").next());
	});
	
	//参数设置
	Area.prototype.set = function(option) {
		var _this = this;
		$.extend(true, _this.areaConfig, option);
		return _this;
	};
	
	Area.prototype.init = function() {
		this.getJSON(this.areaConfig.url,$("select[name='prov']").closest("div"));
	};
	
	Area.prototype.getJSON = function (urls,even){
		var that = this;
		$.getJSON(urls, function(json){
			var pid = 0;
			var name = even.find("select").attr("name");
			var select = "<select name=\"" + name + "\" lay-filter=\"area\">";
			select += "<option value=\"0\">请选择 </option>";
			$(json).each(function(){
				select += "<option value=\"" + this.area_id + "\"";
				if(that.areaConfig.provId == this.area_id || that.areaConfig.cityId == this.area_id || that.areaConfig.distId == this.area_id){
				  select += " selected=\"selected\" ";
				  pid = this.area_id;
				}
				select += ">" + this.area_name + "</option>";
			});
			select += "</select>";
			even.html(select);
			var nextName = even.next().find("select").attr("name");
			even.next().html("<select name=\"" + nextName + "\" lay-filter=\"area\"><option value=\"0\">请选择 </option></select>");
			form.render('select');
			if(pid != 0){
				that.getJSON(that.areaConfig.url+"?area_id="+pid,even.next());
			}
		});
	}
	
	var AreaO = new Area();
	exports("area",function(option){
		AreaO.set(option);
		AreaO.init();
		return ;
	});
	
})



