(function () {
    var dataArray = [];/*本地存储的公共数组*/

    /*页面初始化*/
    if(window.localStorage.getItem('TodoList')){
        dataArray = JSON.parse(window.localStorage.getItem('TodoList'));
        var liStr = '',flag = 0;
        dataArray.forEach(function (val,index) {
            if(val.check == false){
                liStr += setLiStr(val.val);
                flag++;
            }else{
                liStr += setLiStr(val.val,'checked');
            }
        });
        $('ul.todoList').append(liStr);
        !flag && $("input#allCheck").prop('checked',true);
        $('ul.todoList li').length != 0 && $('section.listMain').show();
    }

    /*输入*/
    $('#inp').keydown(function (event) {
        var _this = $(this);
        event.keyCode == 13 && addList(_this);
    });

    /*添加*/
    function addList(_this) {
        var data = htmlEncode(_this.val());
        if(data && data.trim().length!=0) {
            if (!window.localStorage) {/*判断浏览器是否支持该特性*/
                layer.msg("对不起，您的浏览器暂不支持localStorage！");
                return false;
            } else {
                var liStr = setLiStr(data);
                $('ul.todoList li span').length>0?
                    repJudge($('ul.todoList li span'),liStr,data):
                        dataStorage(liStr,data);
                $('section.listMain').show();
                _this.val('');
            }
        }else layer.msg('输入无效！');
    }

    $(document).on('click','ul.todoList a.delBtn',delList);/*删除事件*/
    $(document).on('click','ul.todoList input[type=checkbox]',check);/*选择事件*/
    $(document).on('click','#allCheck',allCheck);/*全选事件*/

    /*判断重复*/
    function repJudge(_spans,liStr,data) {
        var flag = true;
        _spans.each(function (index,val) {
            if($(val).text() == data){flag=false;return false;}
        });
        flag?dataStorage(liStr,data):layer.msg('内容重复！');
    }

    /*所添加的字符串*/
    function setLiStr(val,str) {
        return `<li class="p_r"><input class="p_a" `+str+` type="checkbox"/><span>`+val+`</span><a title="删除" class="p_a delBtn" href="javascript:;"></a></li>`;
    }

    /*本地存储数据添加*/
    function dataStorage(liStr,data) {
        $('ul.todoList').append(liStr);
        dataDeal({'val':data,'check':false},true,1);
        allJudge($('ul.todoList').find('input'),true);
    }

    /*选择*/
    function check() {
        !$(this).is(':checked')?$("input#allCheck").prop('checked',false):allJudge($(this).parent().siblings().find('input'),true);
        dataDeal($(this).parent().index(),$(this).prop('checked'),2);
    }

    /*全选*/
    function allCheck() {
        var inp = $(this).siblings('ul').find('input[type=checkbox]');
        $(this).is(':checked')?inp.prop('checked',true):inp.prop('checked',false);
        dataDeal(0,inp.prop('checked'),4);
    }

    /*todoList删除*/
    function delList() {
        allJudge($(this).parent().siblings().find('input'),true);
        dataDeal($(this).parent().index(),$(this).prop('checked'),3);
        $(this).parent().remove();
        if($('ul.todoList li').length == 0){
            $('section.listMain').hide();allJudge($(this),false);
        }
    }

    /*全选判断*/
    function allJudge(array,type) {
        var flag = true;
        type?array.each(function (index,val) {
                if($(val).prop('checked') == false){flag = false;return false;}
            }):flag = type;
        flag?$("input#allCheck").prop('checked',true):$("input#allCheck").prop('checked',false);
    }

    /*数据处理  修改/删除*/
    function dataDeal(_this,flag,type){
        switch (type){
            case 1:dataArray.push(_this);break;
            case 2:dataArray[_this].check=flag;break;
            case 3:dataArray.splice(_this,1);break;
            default:dataArray.forEach(function (val) {
                        val.check = flag;
                    });break;
        }
        window.localStorage['TodoList']=JSON.stringify(dataArray);
    }

    /*script转义*/
    function htmlEncode(str){
        return $('<span/>').text(str).html();
    }
})();