/**

 @Name：layuiAdmin 公共业务
 @Author：贤心
 @Site：http://www.layui.com/admin/
 @License：LPPL

 */

layui.define(function (exports) {
    var $ = layui.$
        , layer = layui.layer
        , laytpl = layui.laytpl
        , setter = layui.setter
        , view = layui.view
        , admin = layui.admin

    //公共业务的逻辑处理可以写在此处，切换任何页面都会执行
    //……


    // //退出
    // admin.events.logout = function(){
    //   //执行退出接口
    //   admin.req({
    //     url: layui.setter.base + 'json/user/logout.js'
    //     ,type: 'get'
    //     ,data: {}
    //     ,done: function(res){ //这里要说明一下：done 是只有 response 的 code 正常才会执行。而 succese 则是只要 http 为 200 就会执行

    //       //清空本地记录的 token，并跳转到登入页
    //       admin.exit(function(){
    //         location.href = 'user/login.html';
    //       });
    //     }
    //   });
    // };

    var common = {
        /**错误msg提示 */
        ErrorMsg: function (text, func) {
            top.layer.msg(text, {icon: 5}, func);
        },
        /**成功 msg提示 */
        SucMsg: function (text, func) {
            top.layer.msg(text, {icon: 6}, func);
        },
        /**ajax 网络错误 */
        ajaxError: function () {
            top.layer.msg('网络错误,请检查网络是否正常', function () {
                //关闭后的操作
            });
        },
        /**ajax Confirm 对话框 */
        ajaxConfirm: function (title, text, url, param) {
            var that = this;
            var lock = false; //默认未锁定

            layer.confirm(text, {
                title: title,
                resize: false,
                btn: ['确定', '取消'],
                btnAlign: 'c',
                icon: 3,

            }, function (index) {
                if (!lock) {

                    $.ajax({
                        url: url,
                        type: 'post',
                        async: false,
                        data: param,
                        success: function (data) {
                            if (data.status == 'Success') {
                                lock = true; // 锁定
                                that.SucMsg(data.message);
                                location.reload();
                            } else {
                                that.ErrorMsg(data.message);
                                layer.close(index);
                            }
                        }, error: function (data) {
                            that.ajaxError();
                            layer.close(index);
                        }
                    });
                }
            }, function () {

            })

        },
        /**弹出层*/
        layOpen: function (title, url, width, height) {

            var index = layer.open({
                title: title,
                type: 2,
                content: url,
                area: [width, height],
                fix: false, //不固定
                maxmin: true,
                shade: 0.4,
                success: function (layero, index) {

                }
            });
        },
        //大窗口打开页面
        layOpenBig: function (title, url) {
            var index = layer.open({
                title: title,
                // skin : 'layui-layer-lan',
                type: 2,
                content: url
            });
            layer.full(index);
        },
        //图片预览
        imgPreview: function (id, src) {
            if (src == '') return;
            layer.tips('<img src="' + src + '" height="100">', '#' + id, {
                tips: [1, '#fff']
            });
        },
        /**iframe表单ajax提交*/
        formSave: function (url, param, notParent) {
            var notParent = arguments[2] ? true : false;
            var that = this;
            var loading = top.layer.msg('数据提交中，请稍候', {icon: 16, time: false, shade: 0.8});
            $.ajax({
                url: url,
                type: 'post',
                data: param,
                dataType:'json',
                error: function (data) {
                    top.layer.close(loading);
                    that.ajaxError();
                },
                success: function (data) {
                    if (data.status == 'Success') {
                        top.layer.close(loading);
                        that.SucMsg(data.message);
                        var index = parent.layer.getFrameIndex(window.name); //先得到当前iframe层的索引
                        parent.layer.close(index);    //再执行关闭
                        if (notParent) {
                            location.reload();    //刷新当前页面
                        } else {
                            parent.location.reload();   //刷新父页面
                        }
                        console.log(2);
                    } else {
                        top.layer.close(loading);
                        that.ErrorMsg(data.message);
                    }
                }

            });
        },
        /**数据表格批量删除*/
        deleteBatch: function (tableId, url) {
            var that = this;
            var checkStatus = layui.table.checkStatus(tableId)
            var id = '';
            if (checkStatus.data.length === 0) {
                return layer.msg('请选择数据!');
            }
            $.each(checkStatus.data, function (i, e) {
                id += "&id[]=" + e.id;
            })
            var encode = encodeURI(id.substr(1));
            layer.confirm('确认要执行该操作吗?', function (index) {
                $(this).prop('disabled', true);
                $.post(url, encode, function (data) {
                    layer.close(index);
                    $(this).prop('disabled', false);
                    if (data.status != 'Success') {
                        return that.ErrorMsg(data.message)
                    }
                    that.SucMsg(data.message);
                    setTimeout(function () {
                        layui.table.reload(tableId);
                    }, 1500);
                });
            });
            return false;
        },
        /**附件上传*/
        upload_box: function (url) {
            layer.open({
                type: 2,
                title: '上传附件',
                area: ['500px', '430px'],
                content: url,
                btn: ['确定', '取消'],
                yes: function (index, layero) {
                    var iframeWin = window[layero.find('iframe')[0]['name']]; //得到iframe页的窗口对象，执行iframe页的方法：iframeWin.method();
                    iframeWin.determine();
                },
                btn2: function (index, layero) {
                    var iframeWin = window[layero.find('iframe')[0]['name']]; //得到iframe页的窗口对象，执行iframe页的方法：iframeWin.method();
                    iframeWin.cancel();
                }
            });
        },
        showPic: function showPic() {
            layer.ready(function () { //为了layer.ext.js加载完毕再执行
                layer.photos({
                    photos: "#showPic" //格式见API文档手册页
                    , anim: 5 //0-6的选择，指定弹出图片动画类型，默认随机
                });
            });
        }
    };


    //对外暴露的接口
    exports('common', common);
});
